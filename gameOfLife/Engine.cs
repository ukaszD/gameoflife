﻿using gameOfLife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

class Engine
{
    public Cell[,] cells;
    bool circularGameField;
    bool showCellsContext;
    public Engine(bool sCellsContext, bool cGameField)
    {
        this.showCellsContext = sCellsContext;
        this.circularGameField = cGameField;
    }

    public void firstCount(int N)
    {
        cells = new Cell[N, N];

        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                cells[i, j] = new Cell(Brushes.LightGray, 20, i, j, 0);
            }
        }
    }

    public void oscilator(int N)
    {
        cells = new Cell[N, N];

        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {

                cells[i, j] = new Cell(Brushes.LightGray, 20, i, j, 0);

                if(( j == 5 || j==6 || j== 7) && i == 7)
                {

                    cells[i, j].setValue(1);
                }

               
            }
        }
    }

    public void stable(int N)
    {
        cells = new Cell[N, N];

        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {

                cells[i, j] = new Cell(Brushes.LightGray, 20, i, j, 0);

                if ((j == 5 || j == 6) && (i == 7 || i == 8))
                {

                    cells[i, j].setValue(1);
                }


            }
        }
    }


    public void glidder(int N)
    {
        cells = new Cell[N, N];

        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {

                cells[i, j] = new Cell(Brushes.LightGray, 20, i, j, 0);

                if (j == 9 && (i == 7 || i == 8 || i == 9 ) )
                {

                    cells[i, j].setValue(1);
                }

                if (j == 10 && (i == 7 ))
                {

                    cells[i, j].setValue(1);
                }

                if (j == 11 && (i == 8))
                {

                    cells[i, j].setValue(1);
                }


            }
        }
    }


    public void chceckConditions(int i, int j)
    {
        if (cells[i, j].value == 0 && cells[i, j].aliveNeighboursCount == 3)
            cells[i, j].setChangeStateFlag();

        if (cells[i, j].value == 1 && cells[i, j].aliveNeighboursCount > 3)
            cells[i, j].setChangeStateFlag();

        if (cells[i, j].value == 1 && cells[i, j].aliveNeighboursCount < 2)
            cells[i, j].setChangeStateFlag();
    }

    public void countNeighbours(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, minJ].value == 1) cells[i, j].aliveNeighboursCount++;
        if (cells[i, minJ].value == 1) cells[i, j].aliveNeighboursCount++;
        if (cells[maxI, minJ].value == 1) cells[i, j].aliveNeighboursCount++;
        if (cells[minI, j].value == 1) cells[i, j].aliveNeighboursCount++;
        if (cells[maxI, j].value == 1) cells[i, j].aliveNeighboursCount++;
        if (cells[minI, maxJ].value == 1) cells[i, j].aliveNeighboursCount++;
        if (cells[i, maxJ].value == 1) cells[i, j].aliveNeighboursCount++;
        if (cells[maxI, maxJ].value == 1) cells[i, j].aliveNeighboursCount++;

        //ustawienie liczby zywych sąsiadów jako wyswietlana na przycisku
        //cells[i, j].Content = cells[i, j].aliveNeighboursCount;

    }

    public void count(int N)
    {
        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                //resetowanie stanu komórki
                cells[i, j].resetState();

                //jeżeli mają wszytkich sąsiadów ośmiu / komorki w środku
                if (i > 0 && i < N - 1 && j > 0 && j < N - 1)
                {
                    //zliczanie zywych sąsiadów
                    countNeighbours(i, j, i + 1, i - 1, j + 1, j - 1);

                    //warunki
                    chceckConditions(i, j);
                }
                else if (circularGameField)
                {
                    if (j == 0 && i == 0)
                    {
                        countNeighbours(i, j, i + 1, N - 1, j + 1, N - 1);
                        chceckConditions(i, j);
                    }

                    if (j == N - 1 && i == 0)
                    {
                        countNeighbours(i, j, i + 1, N - 1, 0, j - 1);
                        chceckConditions(i, j);
                    }

                    if (j == 0 && i == N - 1)
                    {
                        countNeighbours(i, j, 0, i - 1, j + 1, N - 1);
                        chceckConditions(i, j);
                    }

                    if (j == N - 1 && i == N - 1)
                    {
                        countNeighbours(i, j, 0, i - 1, 0, j - 1);
                        chceckConditions(i, j);
                    }

                    if (j == 0 && i > 0 && i < N - 1)
                    {
                        countNeighbours(i, j, i + 1, i - 1, j + 1, N - 1);
                        chceckConditions(i, j);
                    }

                    if (j == N - 1 && i > 0 && i < N - 1)
                    {
                        countNeighbours(i, j, i + 1, i - 1, 0, j - 1);
                        chceckConditions(i, j);
                    }

                    if (i == 0 && j > 0 && j < N - 1)
                    {
                        countNeighbours(i, j, i + 1, N - 1, j + 1, j - 1);
                        chceckConditions(i, j);
                    }

                    if (i == N - 1 && j > 0 && j < N - 1)
                    {
                        countNeighbours(i, j, 0, i - 1, j + 1, j - 1);
                        chceckConditions(i, j);
                    }
                }
                else
                {
                    cells[i, j].setValue(-1);
                }
            }
        }

        foreach (Cell cell in cells)
        {
            if (cell.changeStateFlag)
            {
                cell.changeState();
            }
            if (showCellsContext)
                cell.Content = cell.value;
        }


    }
}

