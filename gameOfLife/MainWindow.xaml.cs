﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace gameOfLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        class Coord
        {
            public int x;
            public int y;
            public Coord()
            {
                this.x = 0;
                this.y = 0;
            }
            public void clear()
            {
                this.x = 0;
                this.y = 0;
            }
        }
        Coord coord;
        int iteration;
        int N;
        Thread mainThread;
        Engine engine;
        bool stopFlag;

        public MainWindow()
        {
            InitializeComponent();
            iteration = Int32.Parse(textboxIter.Text);
            stopFlag = false;
           
            coord = new Coord();
            mainThread = new Thread(threadMethod);
            N = Int32.Parse(textboxN.Text);

            bool cellVal = cbShowValue.IsChecked == true ? true : false;
            bool cGameField = dbCircularGameField.IsChecked == true ? true : false;
            engine = new Engine(cellVal , cGameField);
            engine.firstCount(N);
            drawMesh();
            
        }

        private void resetEngine()
        {
            iteration = Int32.Parse(textboxIter.Text);
            N = Int32.Parse(textboxN.Text);
            bool cellVal = cbShowValue.IsChecked == true ? true : false;
            bool cGameField = dbCircularGameField.IsChecked == true ? true : false;
            engine = new Engine(cellVal, cGameField);
            stopFlag = true;
            iterationCountLabel.Content = "0";
            mainThread = new Thread(threadMethod);
            if(rbO.IsChecked == true ? true : false)
                engine.oscilator(N);
            else if (rbG.IsChecked == true ? true : false)
                engine.glidder(N);
            else if (rbS.IsChecked == true ? true : false)
                engine.stable(N);
            else
                engine.firstCount(N);

            drawMesh();
        }

        private void drawMesh()
        {
            canvas.Children.Clear();
            for (int j = 0; j < N; j++)
            {
                for (int i = 0; i < N; i++)
                {
                    Canvas.SetLeft(engine.cells[i, j], engine.cells[i, j].size * engine.cells[i, j].positionX);
                    Canvas.SetTop(engine.cells[i, j], engine.cells[i, j].size * engine.cells[i, j].positionY);
                    canvas.Children.Add(engine.cells[i, j]);
                }
            }
        }

        private void redrawMesh(int N)
        {
            engine.count(N);
            drawMesh();
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            iteration = Int32.Parse(textboxIter.Text);
            if (mainThread.ThreadState == System.Threading.ThreadState.Unstarted)
                mainThread.Start();
            stopFlag = false;
        }

        private void resetBtn_Click(object sender, RoutedEventArgs e)
        {
            resetEngine();
        }

        private void stopBtn_Click(object sender, RoutedEventArgs e)
        {
            stopFlag = true;
        }

        private void threadMethod()
        {
            int i = 0;
            while (i <= iteration && !stopFlag)
            {
                Thread.Sleep(TimeSpan.FromSeconds(0.5));
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (ThreadStart)delegate ()
                {
                    redrawMesh(N);
                    iterationCountLabel.Content = i.ToString();
                    i++;
                    Console.WriteLine("working");
                });
            }
            Console.WriteLine("thread terminating gracefully.");
        }

    }
}
