﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace gameOfLife
{
    class Cell : Button
    {
        public int positionX;
        public int positionY;
        public int value;
        public int size;
        public int aliveNeighboursCount;
        public bool changeStateFlag;

        public Cell(Brush color, int size, int coordX, int coordY, int val)
        {
            this.changeStateFlag = false;
            this.aliveNeighboursCount = -1;
            this.positionX = coordX;
            this.positionY = coordY;
            this.Background = color;
            this.value = val;
            this.size = size;
            this.Width = size;
            this.Height = size;
            this.Click += (source, e) =>
            {
                if (this.value == 0)
                {
                    setColor(Brushes.Green);
                    this.value = 1;
                }
                else if(this.value == 1)
                {
                    setColor(Brushes.LightGray);
                    this.value = 0;
                }
                else
                {
                    setColor(Brushes.Red);
                    this.value = 0;
                }
            };
        }

        public void setValue(int value)
        {
            this.value = value;
            if (value == 1)
                setColor(Brushes.Green);
            else if(value == 0)
                setColor(Brushes.LightGray);
            else
                setColor(Brushes.Red);
        }

        public void setColor(Brush color)
        {
            this.Background = color;
        }

        public void setChangeStateFlag()
        {
            this.changeStateFlag = true;
        }

        public void changeState()
        {
            if (this.value == 0)
            {
                this.setValue(1);
            }
            else
            {
                this.setValue(0);
            }
        }

        public void resetState()
        {
            this.changeStateFlag = false;
            this.aliveNeighboursCount = 0;
        }

    }
}
